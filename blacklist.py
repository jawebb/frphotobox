import json
import time
import RFID


from PyQt5.QtWidgets import QComboBox, QHBoxLayout
from qtpy.QtWidgets import (QApplication, QLabel, QWidget,
                            QVBoxLayout, QPushButton)

#### Helpful functions, as they impact GUI I was not able to import them from another file. 

def RFID_scan():
    scanbutton.setText("Stop Scan")   
    while scanning:
        QApplication.processEvents()
        epc = RFID.scan()
        time.sleep(.005)
        if epc not in epc_list and epc:
            print(epc)
            epc_list.append(epc)
            epc_down.addItems([epc])
            epc_down.setCurrentIndex(len(epc_list)-1)

   
def RFID_button():
    global scanning
    scanning = not scanning
    if scanning:
        RFID_scan()
    else:
        scanbutton.setText("Scan")

def add_black():
    global black_list
    if epc_down.currentText() not in black_list:
        black_list.append(epc_down.currentText())
        black_down.addItem(epc_down.currentText())
        save_black()

def del_black():
    text = epc_down.currentText()
    if text in black_list:
        index = black_down.findText(text)
        black_down.removeItem(index)
        black_list.pop(index)
        save_black()

def load_black():
    read_file = open("BlackList.json", "r")
    data = json.load(read_file)
    print(data)
    global black_list
    black_list = data["blacklist"]

def save_black():
    settings_dict = {
    "blacklist": black_list
    }

    write_file = open("BlackList.json", "w")
    json.dump(settings_dict, write_file)
    write_file.close()
    load_black()



#GUI, allows tags to be scanned and added to or removed from black_list
def create_original_window():

    # Initialize application
    global app
    app = QApplication([])

    # Create first window
    label = QLabel('Tags scannen die zu Blacklist hinzugefügt oder gelöscht werden sollen')
    labelblack = QLabel('Blacklist:')
    labelepc = QLabel('Gescannte Tags:')
    
    #Blacklist dropdown
    global black_down
    black_down = QComboBox()
    black_down.addItems(black_list)

    #Scan dropdown
    global epc_down
    epc_down = QComboBox()
    epc_down.addItems(epc_list)

    #scan toggle button
    global scanbutton
    scanbutton = QPushButton('Scan')
    scanbutton.clicked.connect(RFID_button)

    #add to blacklist
    addbutton = QPushButton('Element hinzufügen')
    addbutton.clicked.connect(add_black)

    #clear from blacklist
    delbutton = QPushButton('Element löschen')
    delbutton.clicked.connect(del_black)


    # Create layout and add widgets
    bottom_widget = QWidget()
    bottom_layout = QHBoxLayout()
    bottom_layout.addWidget(scanbutton)
    bottom_layout.addWidget(addbutton)
    bottom_layout.addWidget(delbutton)
    bottom_widget.setLayout(bottom_layout)

    black_widget = QWidget()
    black_layout = QHBoxLayout()
    black_layout.addWidget(labelblack)
    black_layout.addWidget(black_down)
    black_widget.setLayout(black_layout)

    epc_widget = QWidget()
    epc_layout = QHBoxLayout()
    epc_layout.addWidget(labelepc)
    epc_layout.addWidget(epc_down)
    epc_widget.setLayout(epc_layout)

    layout = QVBoxLayout()
    layout.addWidget(label)
    layout.addWidget(black_widget)
    layout.addWidget(epc_widget)
    layout.addWidget(bottom_widget)

    # Apply layout to widget
    widget = QWidget()
    widget.setLayout(layout)
    widget.setWindowTitle(WindowTitle)
    widget.setFixedHeight(200)
    widget.setFixedWidth(600)

    # Show widget
    widget.show()

    #Create window until closed (this means that the application does not have to be closed for new modules!)
    app.exec_()



############################### Main (used for saving variables and paths) ############################### 

#Basically List of adjustable values/paths and calls create_original_window
if __name__ == "__main__":
    #decomment for first use to create black_list
    #global black_list
    #black_list = []
    #save_black()

    #load blacklist
    load_black()
    global epc_list
    epc_list = []

    global scanning 
    scanning = False

    global WindowTitle
    WindowTitle = "Blacklist"

    RFID.init()

    create_original_window()