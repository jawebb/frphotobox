'''
How it should work:
User chooses overal module (R0,R1,R2,R3,R4,R5 or other) and clicks ok
User gives in Directoryname (This is then set fixed and not allowed to change)
Directory is created (Checks if Directory already exists, if yes asks if should be saved in already existing directory)
Pictures are taken until User clicks "ok"
Every JPG is shown and User asked if this picture should be copied
If all pictures are copied programme goes back to start, else user is allowed to take pictures again until happy with resulting pictures
'''

#TODO:
'''
#Schau ob RFID Tag mit Datenbank verbunden (vergleich ob tag existiert und f_text in R1/modul/123 oder so ähnlich ändern)
#Schauen wie/wo Fotos abgespeichert werden und mit Datenbank verküpft sind
#Kommentar und Pass/Fail mit Datenbank verknüpfen
#Kommentar gleichzeitig wie fotos anzeigen
#Fotos zoombar machen, auch vor/zurück??
'''


from argparse import ZERO_OR_MORE
from http.client import TEMPORARY_REDIRECT
import json
import datetime
from logging.handlers import TimedRotatingFileHandler
import os
import glob
import shutil
import sys
from tempfile import tempdir
import time
from tracemalloc import start
from turtle import back
import RFID
import serial
import requests


from PyQt5.QtGui import QPixmap, QPalette
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QComboBox, QHBoxLayout, QInputDialog, QMessageBox, QFormLayout, QCheckBox, QLineEdit, QPlainTextEdit, QScrollArea
from qtpy.QtWidgets import (QApplication, QLabel, QWidget,
                            QVBoxLayout, QPushButton , QDialog)

import itkdb
import json

from urllib.parse import urljoin, urlencode, urlparse, parse_qs
from urllib.request import urlopen
import pathlib


############################### Helpful Functions ############################### 
#returns screen size in pixels
def get_screen_size():
    app = QApplication(sys.argv)
    screen = app.primaryScreen()
    rect = screen.size()
    return rect.width(), rect.height()

#sets position of widgets 
def set_positions():
    width, height = get_screen_size()
    #### Adjust these values if necessary
    global positionx 
    positionx = int(2*width/3+50)
    global positiony 
    positiony = int(2*height/3+50)
    global positionx_save_pic
    positionx_save_pic = int(width/2+400)
    global positiony_save_pic
    positiony_save_pic = int(height/2-250)


#Counts number of files in folder
def count_files(dir):
    dirListing = os.listdir(dir)
    return len(dirListing)

#Gives out error message with error_text
def error_message(error_text):
    error_msgbox = QMessageBox()
    error_msgbox.setWindowTitle(WindowTitle)
    error_msgbox.setText(error_text)
    error_msgbox.exec()

#Checks if folder exists, if not gives error message but creates new folder with this name
def check_folder_exists(dir):
    if not os.path.exists(dir):
        error_message("Achtung: Ordner {} nicht gefunden.\n \nNeuer Ordner wurde erstellt.".format(dir))
        os.makedirs(dir)

#closes application
def close_window():
    app.quit()

def RFID_scan():
    scanbutton.setText("Stop Scan")   
    while scanning:
        QApplication.processEvents()
        epc = RFID.scan()
        time.sleep(.005)
        if epc and epc not in epc_list:
            if use_blacklist and epc in black_list:
                continue #continue goes to start of while loop

            print(epc)
            epc_list.append(epc)
            dropdown.addItems([epc])
            dropdown.setCurrentIndex(len(epc_list))

   

def RFID_button():
    global scanning
    scanning = not scanning
    if scanning:
        try:
            RFID_scan()
        except:
            print("Überprüfe ob RFID scanner angeschlossen")
    else:
        scanbutton.setText("Scan")

def clearList():
    global epc_list
    epc_list = []
    dropdown.clear()
    dropdown.addItem("Name per Hand eingeben/Barcode Scannen")


def exit_settings():
    settings_widget.close()

def load_settings():
    read_file = open("settings_dict.json", "r")
    data = json.load(read_file)
    global img_folder_path
    img_folder_path = data["img_folder_path"]
    global img_folder_path_t
    #find folder in which pictures are saved by camera (on my computer the pics are saved in the folder Pictures in a subfolder with the current date, can be changed though)
    today = str(datetime.date.today())
    today = today.replace("-","_")
    #### Adjust these paths!!! 
    img_folder_path_t = img_folder_path+today
    global save_folder_path
    save_folder_path = data["save_folder_path"]
    global JPG_and_RAW_pics #Set to true if both JPG and CR2 are saved
    JPG_and_RAW_pics = data["JPG_and_RAW_pics"]
    print(save_folder_path, img_folder_path_t, JPG_and_RAW_pics)
    

def save_settings():
    save_folder_path = e1.text()
    img_folder_path = e2.text()
    JPG_and_RAW_pics = e3.isChecked()
    settings_widget.close()

    settings_dict = {
    "save_folder_path": save_folder_path, 
    "img_folder_path": img_folder_path,
    "JPG_and_RAW_pics": JPG_and_RAW_pics,
    }

    write_file = open("settings_dict.json", "w")
    json.dump(settings_dict, write_file)
    write_file.close()
    load_settings()

def load_black():
    read_file = open("BlackList.json", "r")
    data = json.load(read_file)
    global black_list
    black_list = data["blacklist"]
    print(black_list)

def pass_function(pass_input, f_text):
    global module_pass
    if pass_input:
        module_pass = "Passed optical inspection"
    else:
        module_pass = "Failed optical inspection"
    comment_text = comment_box.toPlainText()
    time = str(datetime.datetime.now())
    #change save_folder_path to cernbox url, save file into databank as well
    comment_text = f"{f_text}, {module_pass} \n{time[0:-10]} \n{save_folder_path}\n\n{comment_text}"
    print(comment_text)
    today = str(datetime.date.today())
    today = today.replace("-","_")

    global counter_list

    textfilename = f"{save_folder_path}{f_text}/{f_text}_{today}_pics{counter_list[0]}-{counter_list[1]}.txt"
    with open(textfilename, 'w') as f:
            f.write(comment_text)
    global comment_widget
    comment_widget.close()
    #global scrollArea
    #scrollArea.close()
    global picture_taking
    picture_taking = False
    global startbutton
    startbutton.setText("Auswählen")
    global widget
    widget.setEnabled(True)

    # upload info to database
#    uploadDatabaseInformation(AtSerNum)
    uploadTest(AtSerNum)

def countdown():
    global time_till_login
    global picture_taking
    time_till_login -= 1
    
    if time_till_login < 0:
        time_till_login = 0
    hours, secs_till_login = divmod(time_till_login, 3600)
    min, sec = divmod(secs_till_login, 60)

    timelabel.setText("Verbleibende Zeit bis Neustart notwendig: \t \t \t %02d:%02d:%02d"%(hours,min,sec))#(f"Verbleibende Zeit bis Neustart notwendig: \t \t \t {min}:{sec}")
    
    if time_till_login == 0 and not picture_taking:
        app.quit()

############################### Vital Functions ############################### 



def pass_or_fail(f_text):
    #global scrollArea
    #scrollArea.close()
    try:
        os.system("TASKKILL /IM i_view64.exe /F") #close any previous pictures
    except:
        pass
    qm = QMessageBox()
    ret = qm.question(None, WindowTitle, "Modul in Ordnung?", qm.Yes | qm.No)
    print(f"return: {ret}")
    pass_function((ret == qm.Yes), f_text)
   

def next_picture(jpg_list, new_picture_num, index, f_text = ""):
    # shows next (or previous) picture taken. adjusts available buttons accordingly. finished button only available once all pictures viewed
    # Disconnects needed as otherwise one button press can result in multiple function calls
    global next_button
    global back_button
    global current_picture_index

    current_picture_index += index
    print(current_picture_index, index)
    if current_picture_index <= 0:
        current_picture_index = 0
        back_button.setText("")
        try:
            back_button.clicked.disconnect()
        except:
            pass
    elif current_picture_index >= new_picture_num-1:
        current_picture_index = new_picture_num -1
        next_button.setText("")
        next_button.disconnect()
        back_button.setText("Zurück")

        if new_picture_num != 1:
            back_button.setText("Zurück")
            try:
                back_button.clicked.disconnect()
            except:
                pass
            back_button.clicked.connect(lambda: next_picture(jpg_list, new_picture_num, -1))

    else:
        back_button.setText("Zurück")
        next_button.setText("Weiter")
        try:
            back_button.clicked.disconnect()
        except:
            pass
        try:
            next_button.clicked.disconnect()
        except:
            pass
        back_button.clicked.connect(lambda: next_picture(jpg_list, new_picture_num, -1))
        next_button.clicked.connect(lambda: next_picture(jpg_list, new_picture_num, 1, f_text))
    show_picture(jpg_list[current_picture_index])


def pic_comment(f_text, jpg_list, new_picture_num):
    global comment_box
    comment_box = QPlainTextEdit()
    global next_button
    next_button = QPushButton("Weiter")
    next_button.clicked.connect(lambda: next_picture(jpg_list, new_picture_num, 1, f_text))
    global back_button
    back_button = QPushButton("")
    back_button.clicked.connect(lambda: next_picture(jpg_list, new_picture_num, -1))
    global finished_button
    finished_button = QPushButton("")
    finished_button.setText("Fertig")
    finished_button.clicked.connect(lambda: pass_or_fail(f_text))
   

    # Apply layout to widget
    check_widget = QWidget()
    check_layout = QHBoxLayout()
    check_layout.addWidget(next_button)
    check_layout.addWidget(back_button)
    check_layout.addWidget(finished_button)
    check_widget.setLayout(check_layout)

    layout = QVBoxLayout()
    layout.addWidget(comment_box)
    layout.addWidget(check_widget)
    global comment_widget 
    comment_widget = QWidget()
    comment_widget.setWindowTitle("Kommentar")
    comment_widget.setLayout(layout)
    comment_widget.setGeometry(positionx_save_pic, positiony_save_pic, 400,450)
    next_picture(jpg_list, new_picture_num, 0, f_text)
    # Show widget
    comment_widget.show()


def show_picture(filename):
    try:
        os.system("TASKKILL /IM i_view64.exe /F") #close any previous pictures
    except:
        pass
    print(filename)
    filename = filename.replace(os.sep,"/")
    print(filename)
    
    cwd = os.getcwd()
    dir_temp = filename.rsplit("/", 1)
    print(dir_temp)
    os.chdir(dir_temp[0])
    print()
    os.system(f'start {dir_temp[-1]}')#show picture in windows image viewer
    os.chdir(cwd)

 


#Copies n newest files to new_dir and renames them with counter. Also shows files if they are "JPG" and asks if these should be copied or not
def copy_n_pics(dir, new_dir, n, f_text):
    #so all pictures are already transfered to computer from camera
    time.sleep(1)

    #create temp dir for jpgs to be copied into and viewed 
    global temp_dir 
    temp_dir = "temp"
    if os.path.exists(temp_dir): #if already exists will be deleted
        shutil.rmtree(temp_dir)
    os.makedirs(temp_dir)
    temp_dir +="/"

    #read in all files in dir
    files = glob.glob(dir+'/*')
    #New base of name for pictures
    name = f_text
    #sort all files according to last modified date (.getctime for creation date, didnt work for me)
    sort = sorted(files, key = os.path.getmtime)
    
    #find all files with same name already (e.g. name ="R0_Test" + end of filename:"_1.CR2") and finds largest number from which it counts on
    fileList = [_.split("_")[-1] for _ in [_.split(".")[-2] for _ in os.listdir(new_dir) if _.startswith(name) and _.split(".")[-1] != "txt"]]
    counter = max([int(_) for _ in fileList if _.isdigit()], default=0)

    #Find all files which are same but have different extensions, i.e. ".JPG" and ".CR2"
    pairList = [[i for i in range(n) if sort[-(n-i)].split(".")[-2] == sort[-(n-j)].split(".")[-2]] for j in range(n)]

    #copy n newest files to new_dir in order they were created
    #Ask for each picture if it should be copied, if not, allows for more pictures to be taken again with same name
    global counter_list 
    counter_list = [counter+1]
    
    jpg_list = []
    for i in range(n):
        fileext = sort[-(n-i)].split(".")[-1]
        if fileext == "JPG":
            #show_picture(sort[-(n-i)])
            #save = pic_comment(f_text)
            counter += 1
            shutil.copy2(sort[-(n-i)], new_dir+name+"_"+str(counter)+"."+fileext)

            #for image viewer save jpg into temp folder
            shutil.copy2(sort[-(n-i)], temp_dir+name+"_"+str(counter)+"."+fileext)
            jpg_list.append(temp_dir+name+"_"+str(counter)+"."+fileext)
            #jpg_list.append(sort[-(n-i)])
            if JPG_and_RAW_pics: #if both JPG and CR2 pics are taken then this code allows to only copy those CR2 files of which the corresponding JPG was deemed ok. One cannot show CR2 files directly
                j = [index for index in pairList[i] if index != i][0]
                shutil.copy2(sort[-(n-j)], new_dir+name+"_"+str(counter)+".CR2")
        elif fileext == "CR2":#To not copy twice
            if not JPG_and_RAW_pics:
                counter += 1
                shutil.copy2(sort[-(n-i)], new_dir+name+"_"+str(counter)+"."+fileext)
            

        else:#All other potential files created. Maybe delete for Enduser
            counter += 1
            shutil.copy2(sort[-(n-i)], new_dir+name+"_"+str(counter)+"."+fileext)
    counter_list.append(counter)
    if JPG_and_RAW_pics:
        pic_comment(f_text, jpg_list, int(n/2))
    else:
        pic_comment(f_text, jpg_list, n) 



#Checks if image_folder (i.e. folder where camera saves pictures) exists, waits until pics are taken and gives copy_n_pics the number of new files in image_folder
def move_pics(folder_path, f_text):
    check_folder_exists(img_folder_path_t)

    #count how many files currently in that folder
    picture_num = count_files(img_folder_path_t)
    #wait until all pictures are taken and button is pressed (both ok and x should work, test!)
    msgbox = QMessageBox()
    msgbox.setWindowTitle(WindowTitle)
    msgbox.move(positionx, positiony)
    msgbox.setText("Ok drücken nachdem alle Bilder aufgenommen wurden")
    msgbox.exec()

    #Count new objects (new number-previous number) in picture Folder
    new_picture_num = count_files(img_folder_path_t)-picture_num

    
    global current_picture_index
    current_picture_index = 0
    #Copy and Rename new objects from picture Folder to new Folder
    copy_n_pics(img_folder_path_t, folder_path+'/', new_picture_num, f_text)
    #copy_n_pics(img_folder_path_t, folder_path+'/', 1, f_text)



#Reads in module name and, if a new folder can be created, creates new folder in which pictures will be saved and calls move_pics
#Maybe implement databank access so programm converts RFID Tag into sth like R1/BurnIn/Module16?????
def startDialog():
    global startbutton
    startbutton.setText("In Bearbeitung")
    global widget #initial window
    widget.setEnabled(False)
    global picture_taking
    picture_taking = True
    global name_scan
    try:
        if dropdown.currentIndex() == 0:
            #text, ok = QInputDialog.getText(None, WindowTitle, 'Modul Name \n\n(Nicht "/" oder andere ungültige Zeichen benutzen)')
            text, ok = name_scan.text(), True
            if ok and text:
                if "/" in text:
                    error_message("Fehler: '{}' ist kein valider Ordnername.".format(text))
                else:
                    #Create new Folder with name "Text" in Folder R0, R1 ... other , note how many objects (x) in picture Folder
                    f_text = text
                    
        else: 
            f_text = dropdown.currentText()

        #Here one would implement conversion of RFID Tag to module name, adjust name in copy_n_pics accordingly
        global AtSerNum
        # Check if already ATLAS serial number
        if "20USE" in f_text:
            AtSerNum = f_text
        else:
            AtSerNum = findItemByRFID(f_text)
            f_text = AtSerNum

        if f_text:
            folder_path = save_folder_path+f_text

            #Check if this Folder already exists, if not --> try to make it, if it does --> ask if pictures should be saved in same folder
            #Once Folder in which pics should be copied is found: Move pics
            Already_exists = os.path.isdir(folder_path)
            if not Already_exists:
                #make new folder
                try:
                    os.makedirs(folder_path)
                except:
                    error_message("Fehler: '{}' ist kein valider Ordnername.".format(f_text))
                #try, except, else means that code in else loop is executed if try does not fail
                else:
                    move_pics(folder_path, f_text)


            else:
                qm = QMessageBox()
                ret = qm.question(None, WindowTitle, "Modul Ordner existiert bereits, Bilder diesem Ordner hinzufügen?", qm.Yes | qm.No)
                if ret == qm.Yes:
                    move_pics(folder_path, f_text)
    except Exception as e:
        print(e)
        error_message("Unvorhergesehener Fehler, bitte erneut versuchen.")
        startbutton.setText("Auswählen")
        widget.setEnabled(True)
 

#Initial window, calls startDialog once module is chosen from list and ok is pressed
def create_original_window():

    # Initialize application
    global app
    app = QApplication([])

    # Create first window
    label = QLabel('Neuer Bilder Ordner?')
    
    #Dropdown
    global dropdown
    dropdown = QComboBox()
    dropdown.addItem("Name per Hand eingeben/Barcode Scannen")
    dropdown.addItems(epc_list)

    global name_scan
    name_scan = QLineEdit()
    name_scan.returnPressed.connect(startDialog)


    #if enter is clicked startDialog is performed (main part of programme)

    global scanbutton
    scanbutton = QPushButton('RFID Scan')
    scanbutton.clicked.connect(RFID_button)

    #this button closes application (as does pressing the x)
    global startbutton
    startbutton = QPushButton('Auswählen')
    startbutton.clicked.connect(startDialog)
    startbutton.setAutoDefault(True)

    clearbutton = QPushButton('Liste löschen')
    clearbutton.clicked.connect(clearList)

    #
    settingsbutton = QPushButton('Einstellungen')
    settingsbutton.clicked.connect(settings_window)
    #global time_till_login
    global timelabel
    hours, secs_till_login = divmod(time_till_login, 3600)
    min, sec = divmod(secs_till_login, 60)
    timelabel = QLabel("Verbleibende Zeit bis Neustart notwendig: \t \t \t %02d:%02d:%02d"%(hours,min,sec))
    global session_timer
    session_timer = QTimer()
    session_timer.timeout.connect(countdown)
    session_timer.start(1000)
    

    # Create layout and add widgets
    bottom_widget = QWidget()
    bottom_layout = QHBoxLayout()
    bottom_layout.addWidget(scanbutton)
    bottom_layout.addWidget(startbutton)
    bottom_layout.addWidget(clearbutton)
    bottom_layout.addWidget(settingsbutton)
    bottom_widget.setLayout(bottom_layout)

    layout = QVBoxLayout()
    
    layout.addWidget(label)
    layout.addWidget(dropdown)
    layout.addWidget(name_scan)
    layout.addWidget(bottom_widget)
    layout.addWidget(timelabel)
    

    # Apply layout to widget
    global widget
    widget = QWidget()
    widget.setLayout(layout)
    widget.setWindowTitle(WindowTitle)
    widget.setFixedHeight(160)
    widget.setFixedWidth(450)
    widget.move(positionx, positiony)
    name_scan.setFocus()

    # Show widget
    widget.show()

    #Create window until closed (this means that the application does not have to be closed for new modules!)
    app.exec_()



def settings_window():
    # Create first window
    load_settings()
    global e1
    e1 = QLineEdit()
    e1.setText(save_folder_path)
    global e2
    e2 = QLineEdit()
    e2.setText(img_folder_path)
    global e3
    e3 = QCheckBox("Sowohl JPGs als auch CR2 gespeichert?")
    e3.setChecked(JPG_and_RAW_pics)
    exitbutton = QPushButton('Schließen ohne zu speichern')
    exitbutton.clicked.connect(exit_settings)

    #
    savebutton = QPushButton('Speichern')
    savebutton.clicked.connect(save_settings)

    layout = QFormLayout()
    layout.addRow("Speicher Ordner Pfad:", e1)
    layout.addRow("Bilder Ordner Pfad:", e2)
    layout.addRow(e3)
    layout.addRow(savebutton, exitbutton)
    # Apply layout to widget
    global settings_widget
    settings_widget = QWidget()
    settings_widget.setWindowTitle("Einstellungen")
    settings_widget.setLayout(layout)
    settings_widget.setFixedHeight(150)
    settings_widget.setFixedWidth(450)

    # Show widget
    settings_widget.show()

############################### Karstens part, databank stuff ############################### 


def findItemByRFID(identifier):
    """
    This function will find a component in the databse by its RFID.
    It will return the ATLAS serial number of the found component
    """
    # Bring the possible RFID into the correct format
    testRFID = identifier.upper().replace(" ", "").replace(":", "").replace("_", "").replace("-", "")
    
    # Authenticate user again automatically if session has expired 
    dbClient.user.authenticate()

    # Get a specific component by the RFID
    resp = dbClient.get('listComponentsByProperty', json = { 'filterMap' : {'propertyFilter':[{'code': "RFID", 'operator': "=", 'value': testRFID}] } })

    # iterate over all components
    for component in resp:
        atlasSerialNumber =  component['serialNumber']
        for p in component['properties']:
            if p['code'] == 'RFID':
                currentRFID = p['value']
                currentRFID = currentRFID.upper().replace(" ", "").replace(":", "")
                if testRFID == currentRFID:
                    print(f"Found from ATLAS Serial number {atlasSerialNumber} the RFID {currentRFID}:")
                    msg = QMessageBox()
                    msg.setWindowTitle(WindowTitle)
                    msg.setText("Komponente gefunden:")
                    msg.setInformativeText("Seriennummer: " + component['serialNumber'] + 
                    "<br> Komponententyp: " + component['type']['name'] + " " + component['componentType']['name'])
                    msg.exec()
                    #print(json.dumps(component,indent=2))
                    return atlasSerialNumber
    # If nothing is found, return NONE
    error_message("RFID konnte nicht in DB gefunden werden")
    return



def uploadDatabaseInformation(componentATLASSerialNumber):
    """
    This function adds the information of where the photos are uploaded to to
    the ITk production database.
    For this to work, the Itkdb needs to be installed and the user must be
    authenticated. For this, see:
    https://itk.docs.cern.ch/general/itkdb_installation/
    """
    # Import the ITk database python module
    
    #user = itkdb.core.User(accessCode1=os.environ['ITKDB_ACCESS_CODE1'], accessCode2=os.environ['ITKDB_ACCESS_CODE2'])
    #dbClient = itkdb.Client(user=user, use_eos=True)
    # Instantiate an ITkDB client object
    #dbClient = itkdb.Client(use_eos=True)
    #resp = dbClient.get('getComponent', json = {'component':"20USEN00000016"})
    #resp = dbClient.get('getComponent', json = { propertyFilter:[{code: "RFID",operator: "=",value: "E28011606002111C6BBC76"}]})
    #resp = dbClient.get('getComponent', json = {'component':"20USBSX0000421"})
    
    #print(json.dumps(resp,indent=2))
    AttachmentExists = False
    try:
        # Authenticate user again automatically if session has expired 
        dbClient.user.authenticate()
        comp = dbClient.get('getComponent', json={"component" : componentATLASSerialNumber, "alternativeIdentifier" : False})
        for component in comp["attachments"]:
            if component['title'] == "PhotoboxPhotos":
                AttachmentExists = True
                break
    except Exception as e: 
        error_message(e)

    #data, files = buildDataFiles(kind='image')
    #response = dbClient.post('createComponentAttachment', data=data, files=files)

    # Define a few paths that are needed. This depends on the local installation
    eosBaseURL   = "https://cernbox.cern.ch/index.php/s/4NM0Or4l05ztJUA"
    dirDelimiter = "%2F"
    eosURL       = "https://cernbox.cern.ch/index.php/s/4NM0Or4l05ztJUA"
    eosPath      = "/eos/user/f/frlabs/Photobox/"
    # need space at the end or otherwise the quotation marks will not be recognized
    localPath    = r"C:\Users\Fotobox QC\Photobox_CERNBox\ "
    # delete last space to have correct path again
    localPath = localPath[:-1]

    

    # Import the needed modules


    # Create a local file that contains the web path name to the stored photos
#    filename = localPath+"test.txt"
    filename = eosPath+componentATLASSerialNumber+".txt"
    with open(filename, 'w') as out: out.write(eosURL)
    filenamePath = pathlib.Path(filename)

    if AttachmentExists:
        description = "Added additional photos/commentary"
    else: 
        description = "Link to photos in CERN EOS recorded by the Freiburg Photobox"
    # Here, we create the actual object that we will attach to the database
    dbAttachment = {'component' : componentATLASSerialNumber,
                    'title' : "PhotoboxPhotos",
                    'description': description ,
                    'type' : "file",
                    'url' : filenamePath.name }

    eosFiles = {'data': (eosURL, filenamePath.open('r'), "text/html")}
    try:
        resp = dbClient.post('createComponentAttachment', data = dbAttachment, files = eosFiles )
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("Bilder erfolgreich in DB hochgeladen")
        msg.setWindowTitle(WindowTitle)
        msg.exec()
    except Exception as e: 
        login_successful = False
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Fehler")
        msg.setInformativeText('Exception raised: %s' % e)
        msg.setWindowTitle(WindowTitle)
        msg.exec()
    #print(dbClient._response.status_code)
    print(f"Done uploading information to the database for ATLAS serial number {componentATLASSerialNumber}. Got the status code: {dbClient._response.status_code}")
    #print(json.dumps(response, indent=4))

    return


def uploadTest(componentATLASSerialNumber):
    """
    This function adds the information of where the photos are uploaded to to
    the ITk production database.
    For this to work, the Itkdb needs to be installed and the user must be
    authenticated. For this, see:
    https://itk.docs.cern.ch/general/itkdb_installation/
    """
    # Import the ITk database python module
    
    #user = itkdb.core.User(accessCode1=os.environ['ITKDB_ACCESS_CODE1'], accessCode2=os.environ['ITKDB_ACCESS_CODE2'])
    #dbClient = itkdb.Client(user=user, use_eos=True)
    # Instantiate an ITkDB client object
    #dbClient = itkdb.Client(use_eos=True)
    #resp = dbClient.get('getComponent', json = {'component':"20USEN00000016"})
    #resp = dbClient.get('getComponent', json = { propertyFilter:[{code: "RFID",operator: "=",value: "E28011606002111C6BBC76"}]})
    #resp = dbClient.get('getComponent', json = {'component':"20USBSX0000421"})
    
    #print(json.dumps(resp,indent=2))
    AttachmentExists = False
    try:
        # Authenticate user again automatically if session has expired 
        dbClient.user.authenticate()
        comp = dbClient.get('getComponent', json={"component" : componentATLASSerialNumber, "alternativeIdentifier" : False})
        for component in comp["attachments"]:
            if component['title'] == "PhotoboxPhotos":
                AttachmentExists = True
                break
    except Exception as e: 
        error_message(e)

    #data, files = buildDataFiles(kind='image')
    #response = dbClient.post('createComponentAttachment', data=data, files=files)

    # Define a few paths that are needed. This depends on the local installation
    eosBaseURL   = "https://cernbox.cern.ch/index.php/s/4NM0Or4I05ztJUA"
    dirDelimiter = "%2F"
    eosURL       = "https://cernbox.cern.ch/files/link/public/4NM0Or4I05ztJUA/"
    eosPath      = "/eos/user/f/frlabs/Photobox/"
    # need space at the end or otherwise the quotation marks will not be recognized
    localPath    = r"C:\Users\Fotobox QC\Photobox_CERNBox\ "
    # delete last space to have correct path again
    localPath = localPath[:-1]

    

    # Import the needed modules

    comment_text = comment_box.toPlainText()

    # Create a local file that contains the web path name to the stored photos
#    filename = localPath+"test.txt"
#    filename = eosPath+componentATLASSerialNumber+".txt"
#    with open(filename, 'w') as out: out.write(eosURL)
#    filenamePath = pathlib.Path(filename)

    if AttachmentExists:
        description = "Added additional photos/commentary"
    else: 
        description = "Link to photos in CERN EOS recorded by the Freiburg Photobox"
    true, false = True, False
    # Here, we create the actual object that we will attach to the database
    json_template = {"project": "S", "componentType": "", "code": ""}
    children = []
    print(componentATLASSerialNumber)
    if "USES" in componentATLASSerialNumber:
        json_template["componentType"] = "SENSOR"
        json_template["code"] = "VIS_INSP_RES_MOD_V2"
    elif "USED" in componentATLASSerialNumber:
        json_template["componentType"] = "PWB"
        json_template["code"] = "PICTURE"
        # Get connected PBs
        childs = dbClient.get("getComponent", json={"component": componentATLASSerialNumber})
        for pb in childs["children"]:
            if pb["componentType"]["name"] == "Powerboard" and pb["component"] is not None:
                children.append(pb["component"]["serialNumber"])
    elif "USET" in componentATLASSerialNumber:
        json_template["componentType"] = "HYBRID_TEST_PANEL"
        json_template["code"] = "VISUAL_INSPECTION_RECEPTION"
    else:
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Fehler")
        msg.setInformativeText('Object type not yet implemented. Please contact developers')
        msg.setWindowTitle(WindowTitle)
        msg.exec()
        return
    
    dbAttachment = dbClient.get("generateTestTypeDtoSample", json=json_template)
    dbAttachment["component"] = componentATLASSerialNumber
    dbAttachment["institution"] = "UNIFREIBURG"
    dbAttachment["runNumber"] = "001"
    dbAttachment["date"] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
    if "USES" in componentATLASSerialNumber:
        dbAttachment["properties"] = {"COMMENTS": "", "RUNNUMBER": 1}
        for key in dbAttachment["results"]:
            dbAttachment["results"][key] = None
        dbAttachment["results"]["URLSCRATCHPAD"] = eosURL+componentATLASSerialNumber
        dbAttachment["results"]["DAMAGE_TYPE1"] = comment_text
    elif "USED" in componentATLASSerialNumber:
        dbAttachment["results"]["COMMENT"] = ""
        dbAttachment["results"]["LINK_TO_PICTURE"] = eosURL+componentATLASSerialNumber
    elif "USET" in componentATLASSerialNumber:
        dbAttachment["comments"] = [comment_text + " Images stored: " + eosURL+componentATLASSerialNumber]

#    print(json.dumps(dbAttachment, indent=2))
    # dbAttachment = {"component": componentATLASSerialNumber,
    #                 "testType": "VIS_INSP_RES_MOD_V2",
    #                 "institution" : "UNIFREIBURG",
    #                 "runNumber" : "001",
    #                 "date": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
    #                 "passed" : True,
    #                 "problems" : False,
    #                 "properties": {
    #                   "COMMENTS": " ",
    #                   "RUNNUMBER": 1  
    #                     },
    #                 "results" : {
    #                     "URLSCRATCHPAD" : "Images stored: " + eosURL+componentATLASSerialNumber,#filenamePath.name
    #                     "LOCATION1"   : None,
    #                     "DAMAGE_TYPE1": comment_text,
    #                     "URLS1"      : None, # sData["IMS1_1"],
    #                     "LOCATION2"   : None,
    #                     "DAMAGE_TYPE2": None,
    #                     "URLS2"      : None, # sData["IMS2_1"],
    #                     "LOCATION3"   : None,
    #                     "DAMAGE_TYPE3": None,
    #                     "URLS3"      : None, # sData["IMS3_1"],
    #                     "LOCATION4"   : None,
    #                     "DAMAGE_TYPE4": None,
    #                     "URLS4"      : None, # sData["IMS4_1"],
    #                     "LOCATION5"   : None,
    #                     "DAMAGE_TYPE5": None,
    #                     "URLS5"      : None, # sData["IMS5_1"],
    #                     "LOCATION6"   : None,
    #                     "DAMAGE_TYPE6": None,
    #                     "URLS6"      : None  # sData["IMS6_4"]
    #                     }
    #             }
    # dbAttachmentPanel = {"component": componentATLASSerialNumber,
    #                 "testType": "VISUAL_INSPECTION_RECEPTION",
    #                 "institution" : "UNIFREIBURG",
    #                 "runNumber" : "001",
    #                 "date": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
    #                 "passed" : True,
    #                 "problems" : False,
    #                 "comments": [" Images stored: " + eosURL+componentATLASSerialNumber]
    #             }
    # print(dbAttachmentPanel)
#    eosFiles = {'data': (eosURL, filenamePath.open('r'), "text/html")}

    if len(children) > 0:
        exceptions = []
        for child in children:
            dbAttachment["component"] = child
            try:
                resp = dbClient.post('uploadTestRunResults', json = dbAttachment)#, files = eosFiles )
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setText("Bilder erfolgreich in DB hochgeladen")
                msg.setWindowTitle(WindowTitle)
                msg.exec()
            except Exception as e: 
                login_successful = False
                exceptions.append(e)
        if len(exceptions) > 0:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText("Fehler")
            msg.setInformativeText('Exceptions raised: %s' % exceptions)
            msg.setWindowTitle(WindowTitle)
            msg.exec()
            print("Exceptions raised: %s" % exceptions)
            return
    else:
        try:
            resp = dbClient.post('uploadTestRunResults', json = dbAttachment)#, files = eosFiles )
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Bilder erfolgreich in DB hochgeladen")
            msg.setWindowTitle(WindowTitle)
            msg.exec()
        except Exception as e:
            login_successful = False
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText("Fehler")
            msg.setInformativeText('Exception raised: %s' % e)
            msg.setWindowTitle(WindowTitle)
            msg.exec()
    #print(dbClient._response.status_code)
    print(f"Done uploading information to the database for ATLAS serial number {componentATLASSerialNumber}. Got the status code: {dbClient._response.status_code}")
    #print(json.dumps(response, indent=4))

    return

############################## login window stuff #######################################################

def create_login_window():
    
    # Initialize application
    global login_app
    login_app = QApplication(sys.argv)
    
    global Access_Code_1
    global Access_Code_2
    label1 = QLabel('Access Code 1:')
    Access_Code_1 = QLineEdit()
    Access_Code_1.setEchoMode(QLineEdit.Password) #JW 20/12
    label2 = QLabel('Access Code 2:')
    Access_Code_2 = QLineEdit()
    Access_Code_2.setEchoMode(QLineEdit.Password)
    Access_Code_2.returnPressed.connect(lambda: authentification(Access_Code_1.text(), Access_Code_2.text()))
    
    closebutton = QPushButton('Schließen')
    closebutton.clicked.connect(login_app.quit)
    sendbutton = QPushButton('Anmelden')
    sendbutton.clicked.connect(lambda: authentification(Access_Code_1.text(), Access_Code_2.text()))
    
    # Create layout and add widgets
    bottom_widget = QWidget()
    bottom_layout = QHBoxLayout()
    bottom_layout.addWidget(sendbutton)
    bottom_layout.addWidget(closebutton)
    bottom_widget.setLayout(bottom_layout)
    
    layout = QVBoxLayout()
    layout.addWidget(label1)
    layout.addWidget(Access_Code_1)
    layout.addWidget(label2)
    layout.addWidget(Access_Code_2)
    layout.addWidget(bottom_widget)
    
    # Apply layout to widget
    widget = QWidget()
    widget.setLayout(layout)
    widget.setWindowTitle("Login")
    widget.setFixedHeight(175)
    widget.setFixedWidth(450)
    widget.move(positionx, positiony)
    
    # Show widget
    widget.show()

    #Create window until closed (this means that the application does not have to be closed for new modules!)
    login_app.exec_()


def authentification(_accessCode1, _accessCode2):
    global dbClient
    global user
    global login_successful
#    user = itkdb.core.User(accessCode1=_accessCode1, accessCode2= _accessCode2)
    user = itkdb.core.User(access_code1=_accessCode1, access_code2= _accessCode2) #JW 19/12
    dbClient = itkdb.Client(user = user)
    if dbClient is not None: 
            if _accessCode1 == '' or _accessCode2 == '':
                login_successful = False
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText("Warnung")
                msg.setInformativeText("Access code(s) leer gelassen")
                msg.setWindowTitle("Login")
                msg.exec()
            else:
                try: 
                    dbClient.user.authenticate()
                    login_successful = True
                    msg = QMessageBox()
                    msg.setIcon(QMessageBox.Information)
                    msg.setText("Login erfolgreich")
                    msg.setWindowTitle("Login")
                    msg.buttonClicked.connect(login_app.quit)
                    msg.exec()
                except Exception as e: 
                    login_successful = False
                    msg = QMessageBox()
                    msg.setIcon(QMessageBox.Warning)
                    msg.setText("Fehler")
                    msg.setInformativeText('Requests exception raised: %s' % e)
                    msg.setWindowTitle("Login")
                    msg.exec()
    else: 
        login_successful = False
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Fehler")
        msg.setInformativeText('Keine ITkPDSession verfügbar')
        msg.setWindowTitle("Login")
        msg.exec()



############################### Main (used for saving variables and paths) ############################### 

#Basically List of adjustable values/paths and calls create_original_window
if __name__ == "__main__":
    global use_blacklist
    use_blacklist = False
    if use_blacklist:
        load_black()
    global epc_list
    epc_list = []
    global scanning 
    scanning = False
    global zoom_scale
    zoom_scale = 1
    global picture_taking
    picture_taking = False
    
    AtSerNum = 0
    login_successful = False
    dbClient = None
    user = None

    global time_till_login
    time_till_login = 3*60*60 #s

    load_settings()
    set_positions()

    global WindowTitle
    WindowTitle = "Fotobox"
    try:
        RFID.init()
    except serial.SerialException: 
        error_app = QApplication(sys.argv)
        # error_app.exec_()
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Fehler")
        msg.setInformativeText('Ist der RFID Scanner korrekt angeschlossen?')
        msg.setWindowTitle("RFID-Scanner")
        msg.exec()
        #sys.exit() #JW 19/12
        
    create_login_window()
    
    if login_successful:
        create_original_window()





