import itkdb
import getpass

_accessCode1 = getpass.getpass(prompt="Enter passphrase 1")
_accessCode2 = getpass.getpass(prompt="Enter passphrase 2")

user = itkdb.core.User(access_code1=_accessCode1, access_code2= _accessCode2)
dbClient = itkdb.Client(user = user)

dbClient.user.authenticate()

componentATLASSerialNumber = input("Please scan serial code of sensor... ")

comp = dbClient.get('getComponent', json={"component" : componentATLASSerialNumber, "alternativeIdentifier" : False})

print(comp['sys']['rev'])
#print(comp['flags'])
if comp['flags'] != None:
   print('A flag has already been set, aborting... ')
   quit()

ivTest = False
viTest = False
for test in comp['tests']:
   if test['code'] == 'ATLAS18_IV_TEST_V1':
      if test['testRuns'][0]['institution']['code'] == 'UNIFREIBURG': 
         print(f'IV tests performed locally... \n\n {test}\n\n')
         if test['testRuns'][0]['passed'] == True:
            ivTest = True

   if test['code'] == 'VIS_INSP_RES_MOD_V2': 
      print(f'Visual inspection tests performed locally... \n\n {test}\n\n')
      if test['testRuns'][0]['passed'] == True:
         viTest = True

print(f'IV test passed? {ivTest}, VI test passed? {viTest}')

passedReception = False
flag = 0
if ivTest and viTest:
   passedReception = True

input(f'Passed reception flag to be set to {passedReception}, <enter> to continue, <ctrl+c> to kill... ')

if passedReception:
   flag = 'PASSED_MODULE_RECEPTION'
if not passedReception:
   flag = 'FAILED_MODULE_RECEPTION'

resp = dbClient.post('setComponentFlags', json = {'component': componentATLASSerialNumber, 'flags': [flag], 'revision': comp['sys']['rev']})

print(f"Done uploading information to the database for ATLAS serial number {componentATLASSerialNumber}. Got the status code: {dbClient._response.status_code}")
input("Press <enter> to close window.")
